package com.sqli.formation.parking;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import com.sqli.formation.parking.bay.Bay;
import com.sqli.formation.parking.bay.DisabledBay;
import com.sqli.formation.parking.bay.EnabledBay;
import com.sqli.formation.parking.bay.PedesterianExit;
import com.sqli.formation.parking.printer.ParkingPrinter;

public class Parking {
	private TreeSet<Bay> sortedBays;

	public Parking(int squareSize, List<Integer> pedestrianExits,
			List<Integer> disabledBays) {
		
		sortedBays= buildBays(squareSize, pedestrianExits,
				disabledBays);
	}

	private static TreeSet<Bay> buildBays(int squareSize,
			List<Integer> pedestrianExits, List<Integer> disabledBays) {
		Map<Integer, Bay> bays;
		bays = new HashMap<Integer, Bay>();
		for (int i = 0; i < squareSize; i++) {
			bays.put(i, new EnabledBay(i, pedestrianExits));
		}
		for (Integer bayID : disabledBays) {
			bays.put(bayID, new DisabledBay(bayID, pedestrianExits));
		}
		for (Integer bayID : pedestrianExits) {
			bays.put(bayID, new PedesterianExit(bayID, pedestrianExits));
		}
		return new TreeSet<Bay>(bays.values());
	}

	public int getAvailableBays() {
		int nbOfAvalaibleBays = 0;
		for (Bay bay : sortedBays) {
			if (bay.isEmpty())
				nbOfAvalaibleBays++;
		}
		return nbOfAvalaibleBays;
	}

	public int parkCar(char c) {
		Vehicle vehicle = Vehicle.valueOf(String.valueOf(c));
		return vehicle.parkToBay(sortedBays);
	}

	public boolean unparkCar(int firstCarBayIndex) {
		for (Bay bay : sortedBays) {
			if (bay.unpark(firstCarBayIndex))
				return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return new ParkingPrinter().print(sortedBays);
	}
}


package com.sqli.formation.parking.printer;

import com.sqli.formation.parking.bay.Bay;

public class DisabledBayPrinter implements BayPrinter {

	@Override
	public String print(Bay bay) {
		return bay.isEmpty()?"@":"D";
	}

}

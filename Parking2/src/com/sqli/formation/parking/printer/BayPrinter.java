package com.sqli.formation.parking.printer;

import com.sqli.formation.parking.bay.Bay;

public interface BayPrinter {

	String print(Bay bay);

}

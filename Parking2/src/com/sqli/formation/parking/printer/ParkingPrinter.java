package com.sqli.formation.parking.printer;

import java.util.Set;
import java.util.TreeSet;

import com.sqli.formation.parking.bay.Bay;
import com.sqli.formation.parking.bay.BaysByIndexComparator;

public class ParkingPrinter {

	public String print(TreeSet<Bay> sortedBays) {
		double sideLength = Math.sqrt(sortedBays.size());
		StringBuilder result = new StringBuilder();
		Set<Bay> newSet = new TreeSet<Bay>(new BaysByIndexComparator());
		newSet.addAll(sortedBays);
		StringBuilder line = new StringBuilder();
		int resultLineLength = 0;
		boolean reverse = false;
		for (Bay bay : newSet) { 
			line.append(bay.print());
			resultLineLength++;
			if (resultLineLength % sideLength == 0) {
				if (reverse)
					result.append(line.reverse());
				else
					result.append(line);
				result.append("\n");
				line = new StringBuilder();
				reverse = !reverse;
			}
		}
		result.delete(result.length() - 1, result.length());
		return result.toString();
	}

}

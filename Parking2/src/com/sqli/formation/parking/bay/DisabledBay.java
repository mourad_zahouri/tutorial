package com.sqli.formation.parking.bay;

import java.util.List;

import com.sqli.formation.parking.printer.DisabledBayPrinter;

public class DisabledBay extends Bay {


	public DisabledBay(int index, List<Integer> pedestrianExits) {
		super(index, pedestrianExits);
		this.bayPrinter=new DisabledBayPrinter();
	}

	@Override
	public int parkD() {
		if(!isEmpty)
			return -1;
		isEmpty=false;
		return index;
	}
	
}

package com.sqli.formation.parking.bay;

import java.util.Comparator;

public class BaysByIndexComparator implements Comparator<Bay> {

	@Override
	public int compare(Bay o1, Bay o2) {

		return o1.index.compareTo(o2.index);
	}

}
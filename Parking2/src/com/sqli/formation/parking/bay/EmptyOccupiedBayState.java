package com.sqli.formation.parking.bay;

public interface EmptyOccupiedBayState {
	int park(Bay bay);
}

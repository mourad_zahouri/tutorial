package com.sqli.formation.parking.bay;

public class EmptyBayState implements EmptyOccupiedBayState {

	@Override
	public int park(Bay bay) {
		return bay.index;
	}

}

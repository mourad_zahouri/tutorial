package com.sqli.formation.parking.bay;

import java.util.List;

import com.sqli.formation.parking.printer.EnabledBayPrinter;

public class EnabledBay extends Bay {

	public EnabledBay(int index, List<Integer> pedestrianExits) {
		super(index, pedestrianExits);
		bayPrinter = new EnabledBayPrinter();
	}

	@Override
	public int parkM() {
		if (!isEmpty)
			return -1;
		isEmpty = false;
		return index;
	}

	@Override
	public int parkC() {
		if (!isEmpty)
			return -1;
		isEmpty = false;
		return index;
	}

}

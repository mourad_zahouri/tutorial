package com.sqli.formation.restaurant;

public interface CustomerRequestParser {

	public abstract void parse(String customerRequest);

	public abstract String getOrderName();

	public abstract Customer createCustomer();

}
package com.sqli.formation.restaurant;

import java.util.List;

public interface OrderStrategy {

	void execute(String orderName,int indexOfCustomer,
			List<Customer> customers);
}

package com.sqli.formation.framework;

public class ContextClassNotFoundError extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ContextClassNotFoundError() {
		super("Class not found in the context");
	}
}

package com.sqli.formation.framework;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Context {
	private Map<String, Object> objects;


	private static class SingletonHolder {
		
		private static final Context context = new Context();

		public static Context getContext() {
			return context;
		}
		
	}

	private Context() {
		log("Initializing the context");
		try {
			objects = new TreeMap<String, Object>(String.CASE_INSENSITIVE_ORDER);
			parseConfig();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	private void parseConfig() throws FileNotFoundException,
			ClassNotFoundException, InstantiationException,
			IllegalAccessException {
		File configFile = new File("config.txt");
		Scanner configScanner = new Scanner(configFile);
		String line;
		while (configScanner.hasNext()) {
			line = configScanner.nextLine();
			String name = line.split(" ")[0];
			String className = line.split(" ")[1];
			instantiateObject(name, className);
		}
		configScanner.close();
	}

	private void instantiateObject(String name, String className)
			throws ClassNotFoundException, InstantiationException,
			IllegalAccessException {
		log("instantiating object with name=" + name
				+ " className=" + className);

		Class<?> objectClass = Class.forName(className);
		Object object = objectClass.newInstance();
		objects.put(name, object);
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T getInstance(String name) {
		T t = (T) SingletonHolder.getContext().objects.get(name);
		if(t==null)
			throw new ContextClassNotFoundError();
		return t;
	}
	
	private void log(String logText){
		System.out.println(logText);
	}

}

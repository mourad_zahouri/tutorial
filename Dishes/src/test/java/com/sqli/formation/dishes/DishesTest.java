package com.sqli.formation.dishes;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class DishesTest {

	@Test
	public void RTMShouldBeWellDisplayed() {
		// "id,orientation"
		Dishes dishes = new Dishes("1,7W");
		// "name, SATELLITE"
		assertEquals("|**********|", dishes.signal("RTM, N"));
	}

	@Test
	public void ZDFShouldBeDisplayedBadlyAndEuronewsShouldNotBeDisplayed() {
		Dishes dishes = new Dishes("1,7W", "2,19E");
		assertEquals("No signal !", dishes.signal("Euronews, H"));
		assertEquals("|********..|", dishes.signal("ZDF, A"));
	}

	@Test
	public void shouldFindTheBestQuality() throws Exception {
		Dishes dishes = new Dishes("1,19E", "2,19.3E");
		assertEquals("|*********.|", dishes.signal("ZDF, A"));
	}

	@Test
	public void EuronewsShouldBeDisplayedIfDishIsMoved() {
		Dishes dishes = new Dishes("1,7W", "2,19E");
		dishes.move("2", "6.7W");
		assertEquals("|***.......|", dishes.signal("Euronews, H"));
		dishes.move("2", "0.6E");
		assertEquals("|*********.|", dishes.signal("Euronews, H"));
	}

	@Test
	public void RTMShouldBeDisplayedIfDishIsMoved() {
		Dishes dishes = new Dishes("1,13E");
		dishes.move("1", "20.5W");
		assertEquals("|*****.....|", dishes.signal("RTM, N"));
	}

	/***************************************
	 *************************************** 
	 ** Bonus use cases **
	 *************************************** 
	 ***************************************/
	@Test
	public void doubleHeadedDish() throws Exception {
		Dishes dishes = new Dishes("1,7W", "2,19E,13E");
		assertEquals("|********..|", dishes.signal("ZDF, A"));
		assertEquals("|**********|", dishes.signal("Euronews, H"));
		assertEquals("|**********|", dishes.signal("RTM, N"));
	}

	@Test
	public void doubleHeadedDishCanMove() throws Exception {
		Dishes dishes = new Dishes("1,19E,13E");
		dishes.move("1", "20.5W");
		assertEquals("No signal !", dishes.signal("ZDF, A"));
		assertEquals("|*****.....|", dishes.signal("RTM, N"));
	}

	@Test
	public void tripleHeadedDishCanMove() throws Exception {
		Dishes dishes = new Dishes("1,19E,13E,13.2E");
		dishes.move("1", "20.5W");
		assertEquals("No signal !", dishes.signal("ZDF, A"));
		assertEquals("|*******...|", dishes.signal("RTM, N"));
	}

	@Test
	public void test() throws Exception {
		Dishes dishes = new Dishes("1,7W");
		dishes.move("1", "8W");
		dishes.move("1", "8E");
		assertEquals("|**********|", dishes.signal("RTM, N"));
	}
	
	@Test
	public void multiHeadedDishCanMove() throws Exception {
	Dishes dishes = new Dishes("1,13E");
	dishes.move("1", "6E");
	assertEquals("|********..|", dishes.signal("RTM, A"));
	}

}

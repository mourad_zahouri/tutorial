package com.sqli.formation.dishes.orientation.factory;

import com.sqli.formation.dishes.orientation.Orientation;
import com.sqli.formation.dishes.orientation.OrientationType;

public interface OrientationFactory {
	Orientation createOrientation(double deviationAngle,OrientationType orientationType);
}

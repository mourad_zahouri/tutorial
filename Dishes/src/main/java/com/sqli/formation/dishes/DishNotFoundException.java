package com.sqli.formation.dishes;

public class DishNotFoundException extends RuntimeException {
	private static final String templateMessage = "Dish with ID=%d is not found.";

	public DishNotFoundException(int dishID) {
		super(String.format(templateMessage, dishID));
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}

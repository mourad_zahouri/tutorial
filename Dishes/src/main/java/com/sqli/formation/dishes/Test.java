package com.sqli.formation.dishes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.SortedSet;
import java.util.TreeSet;


public class Test {
	public static void main(String[] args) {
		System.out.println("initialisation");
		List<Double> doubles=new ArrayList<Double>();
		for(int i=0;i<100000;i++){
			doubles.add(new Random().nextDouble()*10);
		}
		simpleCalcul(doubles);
		sortedCalcul(doubles);
		sortedSetCalcul(doubles);
	}

	private static void simpleCalcul(List<Double> doubles) {
		System.out.println("Debut du calcul du max");
		long t1=System.currentTimeMillis();
		double max=0;
		for (Double d : doubles) {
			if(d>max)
				max=d;
		}
		long t2=System.currentTimeMillis();
		long time=(t2-t1);
		System.out.println("Le max est "+max+ " le temps de calcul est "+time);
	}
	private static void sortedCalcul(List<Double> doubles) {
		System.out.println("Debut du calcul du max");
		long t1=System.currentTimeMillis();
		Collections.sort(doubles);
		double max=doubles.get(doubles.size()-1);
		long t2=System.currentTimeMillis();
		long time=(t2-t1);
		System.out.println("Le max est "+max+ " le temps de calcul est "+time);
	}
	private static void sortedSetCalcul(List<Double> doubles) {
		System.out.println("Debut du calcul du max");
		long t1=System.currentTimeMillis();
		SortedSet<Double> setDoubles=new TreeSet<Double>();
		for (Double d : doubles) {
			setDoubles.add(d);
		}
		double max=doubles.get(doubles.size()-1);
		long t2=System.currentTimeMillis();
		long time=(t2-t1);
		System.out.println("Le max est "+max+ " le temps de calcul est "+time);
	}
}

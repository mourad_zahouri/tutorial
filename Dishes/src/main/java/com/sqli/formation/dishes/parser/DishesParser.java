package com.sqli.formation.dishes.parser;

import java.util.List;

import com.sqli.formation.dishes.Dish;
import com.sqli.formation.dishes.factory.DishFactory;
import com.sqli.formation.dishes.orientation.factory.OrientationFactory;
import com.sqli.formation.dishes.orientation.parser.OrientationParser;

public interface DishesParser {

	void parse(String[] dishesInput);

	List<Dish> getDishes();

	void setDishFactory(DishFactory dishFactory);

	void setOrientationFactory(OrientationFactory orientationFactory);

	void setOrientationParser(OrientationParser orientationParser);

}

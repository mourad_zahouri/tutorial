package com.sqli.formation.dishes;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import com.sqli.formation.dishes.orientation.Orientation;
import com.sqli.formation.dishes.satellite.Satellite;

public class Dish {
	private int ID;
	private List<Orientation> orientations;

	public Dish(int iD) {
		super();
		ID = iD;
		orientations = new ArrayList<Orientation>();
	}

	public int calculateSignalQualityToSatellite(Satellite satellite) {
		int maxSignalQuality = calculateMaxSignalQualityToOrientation(satellite
				.getOrientation());
		return maxSignalQuality;
	}

	private int calculateMaxSignalQualityToOrientation(
			Orientation otherOrientation) {
		SortedSet<Integer> signalQualities = new TreeSet<Integer>();
		for (Orientation orientation : orientations) {
			signalQualities.add(orientation
					.calculateSignalQualityToOrientation(otherOrientation));
		}
		return signalQualities.last();
	}
	
	public void moveWithOrientation(Orientation toMoveOrientation) {
		toMoveOrientationAffectOrientations(toMoveOrientation);
	}

	private void toMoveOrientationAffectOrientations(
			Orientation toMoveOrientation) {
		for (Orientation orientation : orientations) {
			toMoveOrientation.affect(orientation);
		}
	}

	public void addOrientation(Orientation orientation) {
		orientations.add(orientation);
	}

	public int getID() {
		return ID;
	}

	@Override
	public String toString() {
		return "Dish [ID=" + ID + ", orientations=" + orientations + "]";
	}

}

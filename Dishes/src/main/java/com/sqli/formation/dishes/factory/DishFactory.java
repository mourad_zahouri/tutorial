package com.sqli.formation.dishes.factory;

import com.sqli.formation.dishes.Dish;
import com.sqli.formation.dishes.orientation.factory.OrientationFactory;

public interface DishFactory {

	Dish createDishWithOrientation(int dishID, double dishDeviation, String dishOrientation);

	void setOrientationFactory(OrientationFactory orientationFactory);

	Dish createDish(int dishID);

}

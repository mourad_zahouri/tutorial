package com.sqli.formation.dishes.signal.printer;

public interface SignalPrinter {
	String printSignalWithValue(int signalValue);
}

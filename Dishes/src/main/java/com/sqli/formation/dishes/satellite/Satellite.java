package com.sqli.formation.dishes.satellite;

import java.util.List;

import com.sqli.formation.dishes.Dish;
import com.sqli.formation.dishes.orientation.Orientation;

public class Satellite {
	Orientation orientation;
	private String reference;

	public Satellite(Orientation orientation, String reference) {
		super();
		this.orientation = orientation;
		this.reference = reference;
	}
	
	public String getReference() {
		return reference;
	}
	
	public Orientation getOrientation() {
		return orientation;
	}

	@Override
	public boolean equals(Object obj) {
		return this.reference.equals(((Satellite) obj).reference);
	}

	@Override
	public String toString() {
		return "Satellite [orientation=" + orientation + ", reference="
				+ reference + "]";
	}

}

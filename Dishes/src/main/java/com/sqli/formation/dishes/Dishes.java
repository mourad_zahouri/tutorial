package com.sqli.formation.dishes;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import com.sqli.formation.context.Context;
import com.sqli.formation.dishes.factory.DishFactory;
import com.sqli.formation.dishes.orientation.Orientation;
import com.sqli.formation.dishes.orientation.OrientationType;
import com.sqli.formation.dishes.orientation.factory.OrientationFactory;
import com.sqli.formation.dishes.orientation.parser.OrientationParser;
import com.sqli.formation.dishes.parser.DishesParser;
import com.sqli.formation.dishes.satellite.Satellite;
import com.sqli.formation.dishes.satellite.SatelliteNotFoundException;
import com.sqli.formation.dishes.satellite.factory.SatelliteFactory;
import com.sqli.formation.dishes.signal.parser.SignalParser;
import com.sqli.formation.dishes.signal.printer.SignalPrinter;

public class Dishes {
	private DishesParser dishesParser = Context.getInstance("DishesParser");
	private SatelliteFactory satelliteFactory = Context
			.getInstance("SatelliteFactory");
	private DishFactory dishFactory = Context.getInstance("DishFactory");
	private SignalPrinter signalPrinter = Context.getInstance("SignalPrinter");
	private OrientationFactory orientationFactory = Context
			.getInstance("OrientationFactory");
	private OrientationParser orientationParser = Context
			.getInstance("OrientationParser");
	
	
	private List<Satellite> satellites = new ArrayList<Satellite>();
	private List<Dish> dishes;

	public Dishes(String... dishesInput) {
		injectDependancies();
		//
		createSatellites();
		createDishesFromInput(dishesInput);
	}

	private void injectDependancies() {
		orientationParser.setOrientationFactory(orientationFactory);
		dishFactory.setOrientationFactory(orientationFactory);
		dishesParser.setOrientationFactory(orientationFactory);
		dishesParser.setOrientationParser(orientationParser);
		dishesParser.setDishFactory(dishFactory);
	}

	private void createDishesFromInput(String... dishesInput) {
		dishesParser.parse(dishesInput);
		dishes = dishesParser.getDishes();
	}

	private void createSatellites() {
		satellites.add(satelliteFactory.createSatellite("A", 19.2,
				OrientationType.E));
		satellites.add(satelliteFactory.createSatellite("H", 13,
				OrientationType.E));
		satellites.add(satelliteFactory.createSatellite("N", 7,
				OrientationType.W));
	}

	public String signal(String signalInput) {
		Satellite toReachSattelite = getToReachSatellite(signalInput);
		SortedSet<Integer> signalQualities=new TreeSet<Integer>();
		for (Dish dish : dishes) {
			signalQualities.add(dish.calculateSignalQualityToSatellite(toReachSattelite));
		}
		int bestQualitySignalValue = signalQualities.last(); 
		return signalPrinter.printSignalWithValue(bestQualitySignalValue);
	}

	private Satellite getToReachSatellite(String signalInput) {
		SignalParser signalParser = Context.getInstance("SignalParser");
		signalParser.parse(signalInput);
		String toReachSatelliteRef = signalParser.getSatelliteReference();
		Satellite toReachSattelite = findSatteliteByReference(toReachSatelliteRef);
		return toReachSattelite;
	}

	public void move(String dishIDString, String toMoveOrientationString) {
		int dishID = Integer.parseInt(dishIDString);
		Dish dishToMove = findDishByID(dishID);
		Orientation toMoveOrientation = orientationParser.parseOrientation(
				toMoveOrientationString).get(0);
		dishToMove.moveWithOrientation(toMoveOrientation);
	}

	private Satellite findSatteliteByReference(String satelliteReference) {
		for (Satellite satellite : satellites) {
			if (satellite.getReference().equals(satelliteReference))
				return satellite;
		}
		throw new SatelliteNotFoundException();
	}

	private Dish findDishByID(int dishID) {
		for (Dish dish : dishes) {
			if (dish.getID() == dishID)
				return dish;
		}
		throw new DishNotFoundException(dishID);
	}
}

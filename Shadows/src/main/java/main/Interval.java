package main;

public class Interval {
	private float begin;
	private float end;
	private float length;
	
	public Interval(float begin, float end) {
		super();
		this.begin = begin;
		this.end = end;
		length=end-begin;
	}

	public boolean canMerge(Interval interval) {
		if (begin <= interval.begin && end >= interval.begin) {
			return true;
		}
		if (end >= interval.end && begin <= interval.end) {
			return true;
		}
		return false;
	}
	public Interval merge(Interval interval) {
		float b = begin, e = end;
		if (b > interval.begin)
			b = interval.begin;
		if (e < interval.end)
			e = interval.end;
		Interval res = new Interval(b, e);
		return res;

	}
	public float getBegin() {
		return begin;
	}


	public void setBegin(float begin) {
		this.begin = begin;
	}


	public float getEnd() {
		return end;
	}


	public void setEnd(float end) {
		this.end = end;
	}


	@Override
	public String toString() {
		return "Interval [begin=" + begin + ", end=" + end + ", length="
				+ length + "]";
	}
	
	
	
	

}

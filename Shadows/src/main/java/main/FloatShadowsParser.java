package main;

import java.util.ArrayList;
import java.util.List;

public class FloatShadowsParser implements ShadowsParser {
	List<Building> buildings=new ArrayList<Building>();
	public void parse(float[] values) {
		if (values.length % 2 != 0)
			throw new RuntimeException();
		for (int i = 0; i < values.length; i+=2) {
			Building building=new Building(values[i], values[i+1]);
			buildings.add(building);
		}
	}

	public List<Building> getBuildings() {
		return buildings;
	}

}

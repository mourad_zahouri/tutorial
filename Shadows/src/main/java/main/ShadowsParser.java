package main;

import java.util.List;

public interface ShadowsParser {

	void parse(float[] values);

	List<Building> getBuildings();

}

package main;

import java.util.List;

public interface ShadowsPrinter {

	public abstract String print(List<Interval> intervals);

}
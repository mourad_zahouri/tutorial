package com.sqli.formation.parking;

public abstract class Bay {
	protected boolean isEmpty = true;

	public abstract boolean park();

	public abstract boolean isEmpty();

	public boolean parkC() {
		return false;
	};

	public boolean parkM() {
		return false;
	};

	public boolean parkD() {
		return false;
	};
}

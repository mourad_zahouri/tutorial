package com.sqli.formation.parking;

import java.util.ArrayList;
import java.util.List;

public class ParkingBuilder {
	private int squareSize;
	private List<Integer> pedestrianExits;
	private List<Integer> disabledBays;
	
	public ParkingBuilder() {
		pedestrianExits=new ArrayList<Integer>();
		disabledBays=new ArrayList<Integer>();
	}

	public ParkingBuilder withSquareSize(int size) {
		this.squareSize=size*size;
		return this;
	}


    public ParkingBuilder withPedestrianExit(final int pedestrianExitIndex) {
    	pedestrianExits.add(pedestrianExitIndex);
		return this;
    }

    public ParkingBuilder withDisabledBay(final int disabledBayIndex) {
    	disabledBays.add(disabledBayIndex);
		return this;
    }

    public Parking build() {
		return new Parking(squareSize, pedestrianExits,disabledBays);
    }
}

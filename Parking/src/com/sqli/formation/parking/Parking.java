package com.sqli.formation.parking;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Parking {
	private int squareSize;
	private Map<Integer, Bay> bays;
	public List<Integer> pedestrianExits;

	public Parking(int squareSize, List<Integer> pedestrianExits,
			List<Integer> disabledBays) {
		this.squareSize = squareSize;
		this.pedestrianExits = pedestrianExits;
		bays = new HashMap<Integer, Bay>();
		for (int i = 0; i < squareSize; i++) {
			bays.put(i, new EnabledBay());
		}
		for (Integer bayID : disabledBays) {
			bays.put(bayID, new DisabledBay());
		}
		for (Integer bayID : pedestrianExits) {
			bays.put(bayID, new PedesterianExit());
		}
		System.out.println(bays.size());

	}

	public int getAvailableBays() {
		int nbOfAvalaibleBays = 0;
		for (Bay bay : bays.values()) {
			if (bay.isEmpty())
				nbOfAvalaibleBays++;
		}
		return nbOfAvalaibleBays;
	}

	public int parkCar(char c) {
		Vehicle vehicle = Vehicle.valueOf(String.valueOf(c));
		int whereToParkID = -1;
		for (Integer pedestrianExitID : pedestrianExits) {
			Bay nextBay = bays.get(pedestrianExitID + 1);
			Bay previousBay = bays.get(pedestrianExitID - 1);
//			if (vehicle.parkToBay(previousBay)) {
//				whereToParkID = pedestrianExitID - 1;
//				break;
//			} else if (vehicle.parkToBay(nextBay)) {
//				whereToParkID = pedestrianExitID + 1;
//				break;
//			}
		}
		return whereToParkID;
	}

}

package com.sqli.formation.playoff.qualifier;

import java.util.TreeSet;

import com.sqli.formation.playoff.Team;

public interface Qualifier {

	TreeSet<Team> getQualifiedTeams(TreeSet<Team> teams);

}

package com.sqli.formation.playoff.qualifier;

import java.util.Iterator;
import java.util.TreeSet;

import com.sqli.formation.playoff.Team;

public class BestTeamsQualifier implements Qualifier {

	private static final int MAXOFSELECTEDBESTTEAMS = 4;

	public TreeSet<Team> getQualifiedTeams(TreeSet<Team> teams) {
		TreeSet<Team> selectedBestTeams = new TreeSet<Team>();
		int nbOfSelectedBestTeams = 0;
		Iterator<Team> teamScoreIterator = teams.iterator();
		while (teamScoreIterator.hasNext()) {
			Team team = (Team) teamScoreIterator.next();
			selectedBestTeams.add(team);
			nbOfSelectedBestTeams++;
			if (nbOfSelectedBestTeams == MAXOFSELECTEDBESTTEAMS)
				break;
		}
		return selectedBestTeams;
	}

}

package com.sqli.formation.playoff;

import java.util.TreeSet;

public interface IPlayoff {
	void setTeamScores(TreeSet<Team> teamScores);
}

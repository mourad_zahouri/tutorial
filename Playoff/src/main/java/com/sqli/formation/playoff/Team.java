package com.sqli.formation.playoff;

public class Team implements Comparable<Team> {
	private static final int winingBonnus = 1;
	private String teamName;
	private int score;
	
	public Team(String teamName, int score) {
		super();
		this.teamName = teamName;
		this.score = score;
	}
//TODO seperate win and increment score
	public void win() {
		score += winingBonnus;
	}

	public String getTeamName() {
		return teamName;
	}

	public int compareTo(Team teamScore) {
		return score > teamScore.score ? -1 : 1;
	}

	@Override
	public boolean equals(Object obj) {
		System.out.println("equals");
		return super.equals(obj);
	}

	@Override
	public String toString() {
		return "" + teamName + ":" + score + "";
	}

}

package com.sqli.formation.playoff;

import java.util.TreeMap;

public class Test {

	public static void main(String[] args) {
		TreeMap<String, Integer> map = new TreeMap<String, Integer>();
		map.put("A", 3);
		map.put("D", 5);
		map.put("C", 1);
		System.out.println(map);
		// prints "{A=3, B=2, C=1}"
	}
}

package com.sqli.formation.playoff.printer;

import java.util.List;

import com.sqli.formation.playoff.Team;

public interface PlayoffLayoutPrinter {

	String[] print(List<Team[]> expectedPlayoffMatches);

}

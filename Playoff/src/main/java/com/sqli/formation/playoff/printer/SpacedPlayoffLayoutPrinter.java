package com.sqli.formation.playoff.printer;

import java.util.List;

import com.sqli.formation.playoff.Team;

public class SpacedPlayoffLayoutPrinter implements PlayoffLayoutPrinter {
	private static final String TEMPLATE = "%s vs %s";

	public String[] print(List<Team[]> expectedPlayoffMatches) {
		String[] expectedPlayoffMatchesResult = new String[expectedPlayoffMatches
				.size()];
		int resultIndex = 0;
		for (Team[] teamScores : expectedPlayoffMatches) {
			String strongestTeamName = teamScores[0].getTeamName();
			String weakestTeamName = teamScores[1].getTeamName();
			expectedPlayoffMatchesResult[resultIndex] = String.format(TEMPLATE,
					strongestTeamName, weakestTeamName);
			resultIndex++;
		}
		return expectedPlayoffMatchesResult;
	}

}
